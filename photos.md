title: Pictures!
---

You weren’t there and would like to see what it was like?  You _were_
there and have become nostalgic already?  Here are a few pictures of the
Ten Years of Guix event, 16–18 September 2022, Paris, that should
satisfy your needs.

> _The following photographs by Christopher Baines are published under
> [CC0](https://creativecommons.org/publicdomain/zero/1.0/)._

![Friday group photo around the cake.](/static/images/photos/2022_0916_15334700.small.jpg)

![The cake!](/static/images/photos/2022_0917_15530400.small.jpg)

![Saturday group photo around the second cake.](/static/images/photos/2022_0917_15571400.small.jpg)

> _Photograph below by Morane Gruenpeter published under
> [CC0](https://creativecommons.org/publicdomain/zero/1.0/)._

![Friday cake in front of the street.](/static/images/photos/morane-cake.jpg)

> _Photograph below by Alexandre Dehne Garcia published under
> [WTFPLv2](http://www.wtfpl.net)._

![Friday cake being sliced.](/static/images/photos/IMG_20220917_170250.small.jpg)

> _Photograph below by Julien Lepiller published under
> [CC0](https://creativecommons.org/publicdomain/zero/1.0/)._

![“Error in the finalization thread: Success” (reference to an infamous Guix System message at boot).](/static/images/photos/roptat-finalization-thread.jpg)

> _If you have pictures of the event that you’d like to share on this
> page, please email `guix-birthday-event@gnu.org`._
