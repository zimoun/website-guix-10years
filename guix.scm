;;; Released under the GNU GPL version 3 or (at your option) any later
;;; version.
;;;
;;; Copyright © 2017, 2019-2022 Inria

;; Run 'guix build -f guix.scm' to build the web site.

(use-modules (guix) (gnu)
             (guix modules)
             (guix git-download)
             (gnu packages base)
             (srfi srfi-19)
             (ice-9 match))

(define haunt
  (specification->package "haunt"))

(define haunt&co
  (match (package-transitive-propagated-inputs haunt)
    (((labels packages . _) ...)
     (cons haunt packages))))

(define this-directory
  (dirname (current-filename)))

(define source
  (local-file this-directory "ten-years-of-guix-web"
              #:recursive? #t
              #:select? (git-predicate this-directory)))

(define build
  (with-imported-modules (source-module-closure
                          '((guix build utils)))
    #~(begin
        (use-modules (guix build utils))

        ;; For Haunt.
        (setenv "GUILE_LOAD_PATH"
                (string-join
                 '#+(map (lambda (package)
                           (file-append package "/share/guile/site/"
                                        (effective-version)))
                         haunt&co)
                 ":"))

        ;; So we can read/write UTF-8 files.
        (setenv "GUIX_LOCPATH"
                #+(file-append glibc-utf8-locales "/lib/locale"))
        (setenv "LC_ALL" "en_US.utf8")         ;for the 'haunt' child process
        (setlocale LC_ALL "en_US.utf8")        ;for 'copy-recursively' below

        (copy-recursively #$source ".")

        (invoke #+(file-append haunt "/bin/haunt") "build")
        (mkdir-p #$output)
        (copy-recursively "site" #$output))))

(let ((suffix (if (time>? (current-time time-utc)
                          (date->time-utc
                           (make-date 0 0 0 10 19 09 2022 7200)))
                  ""

                  ;; Trick to force a rebuild every hour, even if nothing
                  ;; changed, so that the time-dependent bits of live.sxml
                  ;; are taken into account.
                  (string-append "-"
                                 (date->string (time-utc->date
                                                (current-time time-utc))
                                               "~Y~m~d~H")))))
  (computed-file (string-append "ten-years-of-guix-web-site" suffix)
                 build))
