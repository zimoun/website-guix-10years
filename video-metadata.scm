#!/bin/sh
exec guile --no-auto-compile "$0" "$@"
!#
;;; This module is part of the Ten Years of Guix web site and is licensed
;;; under the same terms, those of the GNU GPL version 3 or (at your option)
;;; any later version.
;;;
;;; Copyright © 2022 Ludovic Courtès <ludo@gnu.org>

(add-to-load-path (dirname (assq-ref (current-source-location) 'filename)))

(use-modules (guix build utils)
             (program)
             (srfi srfi-1)
             (srfi srfi-19)
             (srfi srfi-26)
             (ice-9 match)
             (ice-9 ftw))

(define destination
  "/tmp/videos-with-metadata")

(define (add-metadata talk source)
  (define (normalize str)
    (string-map (match-lambda
                  (#\newline #\space)
                  (chr chr))
                str))

  (let ((target (string-append destination "/" (talk-video talk))))
    (mkdir-p (dirname target))
    (false-if-exception (delete-file target))
    (invoke "ffmpeg" "-i" source
            "-metadata"
            (string-append "title=" (normalize (talk-title talk)))
            "-metadata"
            (string-append "artist="
                           (normalize (string-join
                                       (map speaker-name
                                            (talk-speakers talk))
                                       ", ")))
            "-metadata"
            (string-append "date="
                           (date->string (talk-date talk) "~Y-~m-~d ~H:~M"))
            "-metadata" "year=2022"
            "-metadata" "album=Ten Years of Guix"
            "-metadata" "copyright=CC-BY"
            "-metadata" "language=en"
            "-c:v" "copy" "-c:a" "copy" target)))


(match (command-line)
  ((_ video-directory)
   (for-each add-metadata
             (filter talk-video talks)
             (map (cut string-append video-directory "/" <>)
                  (scandir video-directory
                           (lambda (file)
                             (string-suffix? ".mp4" file))))))
  ((command . _)
   (format (current-error-port) "Usage: ~a VIDEO-DIRECTORY

Process .mp4 files from VIDEO-DIRECTORY, copying them to ~a and
adding metadata.~%"
           (basename command) destination)))
