;;; This module is part of the Ten Years of Guix web site and is licensed
;;; under the same terms, those of the GNU GPL version 3 or (at your option)
;;; any later version.
;;;
;;; Copyright © 2022 Ludovic Courtès <ludo@gnu.org>

(define-module (program)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-9)
  #:use-module (srfi srfi-19)
  #:use-module (ice-9 match)
  #:export (speaker?
            speaker-name
            speaker-title
            speaker-affiliation

            talk?
            talk-title
            talk-speakers
            talk-synopsis
            talk-duration
            talk-date
            talk-end-date
            talk-slides
            talk-video

            talk-id
            talk-day-predicate
            speakers->sxml
            talk->sxml
            talk->video-page
            morning-talk?
            full-length-talk?
            lightning-talk?
            friday-talk?
            saturday-talk?
            sunday-talk?
            talk-range-predicate
            conjoin

            talks))

(define-record-type <speaker>
  (speaker name title affiliation)
  speaker?
  (name        speaker-name)
  (title       speaker-title)
  (affiliation speaker-affiliation))

(define-record-type <talk>
  (make-talk title speakers synopsis duration date slides video)
  talk?
  (title       talk-title)
  (speakers    talk-speakers)
  (synopsis    talk-synopsis)
  (duration    talk-duration)                     ;minutes
  (date        talk-date)                         ;start time
  (slides      talk-slides)
  (video       talk-video))

(define* (talk title speakers synopsis duration date
               #:optional slides #:key video)
  (make-talk title speakers synopsis duration date slides video))

(define (date str)
  ;; XXX: Force CEST (offset: +7200).
  (string->date str "~Y-~m-~d ~H:~M"))

(define (talk-end-date talk)
  "Return the end date of TALK."
  (let* ((start    (talk-date talk))
         (duration (make-time time-duration 0
                              (* 60 (talk-duration talk)))))
    (time-utc->date
     (add-duration (date->time-utc start) duration))))

(define (talk-earlier? t1 t2)
  (time<? (date->time-utc (talk-date t1))
          (date->time-utc (talk-date t2))))

(define talks
  (sort
   (list (talk "Software Heritage to the rescue of reproducible science"
               (list (speaker "Antoine R. Dumont"
                              "engineer" "Software Heritage")
                     (speaker "Valentin Lorentz" "engineer" "Software Heritage"))
               '((em "This talk is not recorded but you can read its "
                     (a (@ (href "/static/slides/05-lorentz-dumont.md"))
                        "transcript") ".")
                 (p (a (@ (href "https://www.softwareheritage.org")) "Software
Heritage") " is the universal source code archive. It enables
reproducible research and build tools (such as Guix and Nix) by storing
source code forever with a public API, which can be used as a fallback when
original software artifacts are deleted or unavailable. In this talk, we will
discuss how Software Heritage performs global deduplication using a Merkle
DAG, and some planned developments, such as reconstructing original tarballs
exactly using Guix’s "
(a (@ (href "https://ngyro.com/software/disarchive.html"))
   "Disarchive") "."))
               30
               (date "2022-09-16 12:00")
               "05-lorentz-dumont.pdf")
         (talk "Guix as a tool for computational science"
               (list (speaker "Konrad Hinsen"
                              "researcher"
                              "Centre de Biophysique Moléculaire, CNRS
Orléans; Synchrotron SOLEIL, Saint Aubin"))
               "Guix is described as a package manager and a GNU/Linux distribution.
While technically correct, this summary hides the fact that Guix is a
valuable support tool for reproducible computational research, as an add-on
to any GNU/Linux distribution, much like many of us currently use Docker
containers.  I will attempt to convince you that Guix is a better choice than
Docker, and show you how to get started with integrating Guix into your
computational work environment."
               30
               (date "2022-09-16 11:30")
               "04-hinsen.pdf"
               #:video "04-hinsen.mp4")
         (talk "Using Guix for scientific, reproducible, and publishable
experiments"
               (list (speaker "Philippe Swartvagher"
                              "PhD candidate"
                              "Inria"))
               "Ensuring experiment reproducibility with complex software stack can be quite
a headache.  This presentation will tell the story of a PhD student
discovering Guix to ease the reproducibility of the software environments for
scientific experiments.  We will cover the motivation to use Guix, the
features that help to easily customize software environments to fit the
experiment requirements, and then how to share experiment scripts in
scientific publications to be able to reproduce the experiments."
               30
               (date "2022-09-16 14:30")
               "07-swartvagher.pdf"
               #:video "07-swartvagher.mp4")
         (talk "Maneage: a proof-of-concept for rigorous reproducible research
paper criteria"
               (list (speaker "Boud Roukema"
                              #f
                              "Institute of Astronomy, Nicolaus Copernicus
University"))
               '("Maneage is a proof-of-concept implementation of a set of eight criteria for
long-term reproducible scientific research papers. The criteria are:
completeness; modularity; minimal complexity; scalability; verifiable inputs
and outputs; recorded history; linking narrative to analysis; and free and
open-source software. The criteria will be briefly presented.  An outline
of " (a (@ (href "https://maneage.org")) "Maneage")
" with its structure primarily based on bash, make, Git and LaTeX, and
practical experience using and developing it for peer-reviewed papers, will
be presented.")
               30
               (date "2022-09-16 10:00")
               "02-roukema.pdf"
               #:video "02-boukema.mp4")
         (talk "Camlboot: debootstrapping the OCaml compiler"
               (list (speaker "Nathanaëlle Courant"
                              #f
                              "OCamlPro, Inria"))
               '("In this talk, I will present "
                 (a (@ (href "https://github.com/Ekdohibs/camlboot/")) "Camlboot")
                 ", a project which debootstraps the OCaml compiler, that is, is able to
compile the OCaml compiler without using the bootstrap binary. Camlboot
consists in a naïve compiler for a subset of OCaml called MiniML, and an
interpreter for OCaml written in MiniML.  I will first justify the interest
of debootstrapping, then explain the architecture and parts of Camlboot, and
finally present the experimental validation of Camlboot.")
               30
               (date "2022-09-16 15:00")
               "08-courant.pdf"
               #:video "08-courant.mp4")

         (talk "Reproducibility of bioinformatics pipelines"
               (list (speaker "Sarah Cohen-Boulakia"
                              "Professor"
                              "Université Paris-Saclay, LISN"))
               '((p"Impossibility to redo a given analysis that run on the same
computer a few months ago, failing to re-execute the data analysis described
in a peer’s recent paper: recently, we have all experienced computational
reproducibility issues.")
                 (p "The bioinformatics community had to face with the reproducibility crisis and
it has been pioneer in the design of elements of solutions to better
reproduce and reuse bioinformatics pipelines.")
                 (p "In this talk, we will introduce and discuss such elements of solutions and
present lessons learnt in terms of good practices to follow when analyzing
large scientific datasets. "))
               30
               (date "2022-09-16 09:30")
               "01-cohen-boulakia.pdf"
               #:video "01-cohen-boulakia.mp4")
         (talk "Archive, reference, describe and cite software source code: a pathway to
reproducibility"
               (list (speaker "Morane Gruenpeter"
                              "software engineer and project manager"
                              "Software Heritage, Inria"))
               '((p "Software is a fundamental pillar of modern scientific research, and it is
essential to properly archive and reference the source code of software
used in all research activities.")
                 (p "In this presentation, we will show how to use the "
                    (a (@ (href "https://www.softwareheritage.org"))
                       "Software Heritage")
                    " framework to transparently archive all software source code and how to
obtain and use the intrinsic identifiers, specially designed for software
source code, which are necessary to reference the source code in a way that
facilitates long-term reproducibility. We will also show how to describe the
source code for a better understanding of the software and to allow discovery
of the software on search engines. Finally, we will discuss the challenges
when it comes to citing software to give credit to authors in the academic
ecosystem."))
               30
               (date "2022-09-16 14:00")          ;before 4PM
               "06-gruenpeter.pdf"
               #:video "06-gruenpeter.mp4")
         (talk "Helping research on distributed systems with a functional
package manager"
               (list (speaker "Olivier Richard"
                              #f
                              "Inria Grenoble – Rhône-Alpes, LIG"))
               '((p "
Development of environments for distributed systems is a tedious and
time-consuming iterative process. The
reproducibility of such environments is a crucial factor for rigorous
scientific contributions.")
                 (p "
Based on the " (a (@ (href "https://nixos.org/nix")) "Nix")" functional
 package manager we propose a tool that
generates reproducible distributed environment. Moreover, it enables users
to deploy their environments on virtualized (Docker, QEMU) or
physical (Grid’5000) platforms with the same unique description of the
environment.")
                 (p "
After the presentation of the tool and its benefits, limitations and
lessons learned we will be discussed."))
               30
               (date "2022-09-16 11:00")
               "03-richard.pdf"
               #:video "03-richard.mp4")

         (talk "GNU Guix and the RISC-V future"
               (list (speaker "Pjotr Prins"
                              "associate professor"
                              "University of Tennessee Health Science
Center"))
               '((p "RISC-V is a modern open hardware platform that is not only part of many new
devices, but also powering up new areas of research that will lead to
optimised hardware solutions for offloading tasks to dedicated modules on
systems-on-chip (SOCs). The European Union, the National Science Foundation
(NSF) in the US, and NLnet foundation recognise this promise and fund the
design and development of new solutions, including for high-performance
computing (HPC).")

                 (p "
GNU Guix, because of its hackability, flexibility, determinismistic output,
and potential for ‘generics’, makes an ideal partner for RISC-V development
and deployment.  In principal the full software stack with all dependencies
can be ‘carved in stone’ and provide a reproducible design of RISC-V hardware
all the way from idea to taping out a chip, via the stages of simulation,
emulation and testing.")

                 (p "
In this presentation I'll talk about this future of open hardware
architecture that allows for ‘burning software’ into hardware and how
GNU Guix can play a central role in a new industry."))
               30
               (date "2022-09-16 15:30")
               "09-prins.org"
               #:video "09-prins.mp4")


         ;; ☇ Lightning talks. ☇

         (talk "Building a library: from Autotools to Guix"
               (list (speaker "Evgeny Posenitskiy" #f "LCPQ, CNRS"))
               ""
               10
               (date "2022-09-16 16:10")
               "10-posenitskiy.pdf"
               #:video "10-posenitskiy.mp4")
         (talk "Guix quirks when packaging a C GNU Autotools
program distributing also a Python package"
               (list (speaker "Philippe Swartvagher"
                              "PhD candidate"
                              "Inria"))
               ""
               10
               (date "2022-09-16 16:20")
               "11-swartvagher.pdf"
               #:video "11-swartvagher.mp4")
         (talk "Taming the Python"
               (list (speaker "Alice Brenon" #f
                              "ICAR [UMR 5191], LIRIS [UMR 5205]"))
               ""
               10
               (date "2022-09-16 16:30")
               "12-brenon.pdf"
               #:video "12-brenon.mp4")
         (talk "Demo: a malleable GUI for Guix"
               (list (speaker "Konrad Hinsen"
                              "researcher"
                              "Centre de Biophysique Moléculaire, CNRS
Orléans; Synchrotron SOLEIL, Saint Aubin"))
               ""
               10
               (date "2022-09-16 16:40")
               #:video "13-hinsen.mp4")


         ;; Saturday.

         (talk "Towards building modern Android as a Guix package"
               (list (speaker "Julien Lepiller" #f #f))
               "In this talk, I will present the current effort for packaging various
Android parts. I will briefly mention what works (not much), and the future
challenges that I can see coming on that front. The talk will mostly serve to
popularize knowledge about build systems and some of Guix internals, applied
to what was needed to package Android tools."
               30
               (date "2022-09-17 15:30")
               "07-lepiller.pdf"
               #:video "20-lepiller.mp4")

         (talk "Let's translate Guix together!"
               (list (speaker "Julien Lepiller" #f #f))
               "Code is not the only way to contribute.  You're not a native English
speaker?  You can help popularize Guix around you by translating it to your
native language. After a short presentation of the infrastructure that was
put into place for localisation, I will guide you through your first
contribution. We will spend most of the session translating and discussing
various language-related topics."
               30
               (date "2022-09-17 09:30")
               "01-lepiller.pdf"
               #:video "14-lepiller.mp4")

         (talk "How to make GNU Guix irresistible in 2022 and beyond"
               (list (speaker "David Wilson" #f "System Crafters"))
               "
In this talk, we will discuss a number of ideas for how to make Guix
more approachable and widen its userbase of friendly hackers.  Guix is
in a very unique position to be the GNU/Linux distro of choice for Lisp
enthusiasts and configuration tinkerers.  Let's discuss how we can make
the idea of using Guix irresistible and reduce the friction for new
adopters!"
               45
               (date "2022-09-17 11:30")
               "03-wilson.org"
               #:video "16-wilson.mp4")

         (talk "An introduction to Guix Home"
               (list (speaker "David Wilson" #f "System Crafters"))
               '((p "
Guix Home is an exciting new feature of GNU Guix that enables you to
apply the same functional configuration style you enjoy from Guix
System to your own home directory!  If you've ever wondered how to
manage your dotfiles with Guix, this is the tool for you.  It even
works on all Guix-supported GNU/Linux distributions!")
                 (p "
In this talk, I'll explain how it works and how you can get started with it
without fear of breaking your " (code "$HOME") " directory."))
               30
               (date "2022-09-17 14:30")
               "05-wilson.org"
               #:video "18-wilson.mp4")

         (talk "Ten years of failures"
               (list (speaker "Andreas Enge" #f "Inria"))
               "
Ten years are a long time, and an occasion to look back. As any past,
that of Guix is strewn with struggles to do the right thing™ and inevitable
failures, disappointments and broken dreams. Since I am provided the stage,
I will give a personal account of the history of Guix, and who knows, maybe
we will even celebrate a few successes!"
               45
               (date "2022-09-17 10:15")
               "02-enge.pdf"
               #:video "15-enge.mp4")
         (talk "How Replicant, a 100% free software Android distribution,
uses Guix"
               (list (speaker "Denis “GNUtoo” Carikli" #f #f))
               '((a (@ (href "https://replicant.us")) "Replicant") " \
is a 100% free software Android distribution certified by the FSF.  This
short talk will look at how and why Replicant uses or depends on Guix, and
future directions with the usage of Guix by the Replicant project.")
               30
               (date "2022-09-17 15:00")
               "06-carikli.pdf")

         (talk "Contributing to the Guix Data Service as an Outreachy intern"
               (list (speaker "Danjela Lura" #f #f))
               '((p "During the summer of 2020, I had the opportunity to work
on the " (a (@ (href "https://data.guix.gnu.org")) "Guix Data
Service") " as an "
(a (@ (href "https://outreachy.org")) "Outreachy") " intern under the
mentorship of Christopher Baines.  The internship project
was aimed at improving the internationalization support for the Guix Data
Service and thus providing non-English speaking users a more feasible way of
interacting with Guix data."

(p "In this talk, I will walk you through my experience of working on the Guix
Data Service, how it helped me grow both professionally and personally, and I
will also provide my suggestions for other potential interns or
contributors.")))
               30
               (date "2022-09-17 14:00")
               "04-lura.pdf"
               #:video "17-lura.mp4")
         (talk "Guix Europe, how to support the Guix project"
               (list (speaker "Simon Tournier" #f #f))
               '((p "In this talk, we will present the non-profit
association " (a (@ (href "http://foundation.guix.info")) "Guix Europe")
".  We will expose its main goals, showcase some past activities, explain how
to become a member, and present what we plan to achieve with your help."))
               15
               (date "2022-09-17 16:00")
               "08-tournier.org"
               #:video "21-tournier.mp4")

         ;; Sunday.

         (talk "Guixy Guile & the Derivation Factory: a tour of the Guix
source tree"
               (list (speaker "Josselin Poiret" #f #f))
               '((p "Did you ever wonder what kind of magic lies inside the "
                    (code "guix/") " directory?  Do
you want to help untangle the obscure Guix bugs that appear on the mailing
lists, but don't know where to start?  Or do you just want to know your
package manager of choice better?  This talk will hopefully achieve all of
the above, by diving deep and introducing the structure and contents
of (most) of the Guix tree, so that you can find your way back the next time
someone claims an obscure Guix feature is broken!"))
               30
               (date "2022-09-18 10:30")
               "02-poiret.org"
               #:video "27-poiret.mp4")

         (talk "How is that RISC-V port going?"
               (list (speaker "Efraim Flashner" #f #f))
               "
Like the early days of AArch64, 64-bit RISC-V boards are hard to come by.
Everyone's really excited for boards and devices and machines to be
generally available and already making plans for what to do with them.
So where is Guix in all of this? How prepared are we for 64-bit RISC-V?
We'll fill you in with how the port is going, what's left to do, and
where we need help."
               30
               (date "2022-09-18 11:00")
               "03-flashner.pdf"
               #:video "28-flashner.mp4")

         (talk "Using Vim for Guix development"
               (list (speaker "Efraim Flashner" #f #f))
               "
Guix has been called “the Emacs of operating systems” and luckily it does
come with a text editor, vim! We'll work together and see how with just
a few lines of Vim configuration and an extra package or two you'll have
a nice set of tools ready to use to get more from your vim and Guix
installations."
               30
               (date "2022-09-18 14:00")
               #:video "30-flashner.mp4")

         (talk "Progress on automating parts of Guix patch review"
               (list (speaker "Christopher Baines" #f #f))
               '((p "I'll summarise how I review patches, and how I think automating parts of the
process can speed up getting patches merged, while also maintaining or
improving the quality of Guix. I'll also discuss the technical approach I've
been taking to automate parts of reviewing patches and the path forward.")
                 (p "Personally, I feel that patch review needs to not be limited to those who
have the access to merge patches, so this topic and talk should be of
interest to anyone with an interest in Guix."))
               30
               (date "2022-09-18 11:30")
               "04-baines.pdf"
               #:video "29-baines.mp4")

         (talk "A non-programmer perspective on using Guix" ;tentative title
               (list (speaker "Laszlo Krajnikovszkij" #f #f))
               "This talk will present a non-programmer's perspective on using Guix and its
features, as well as other related topics.  Being a very powerful tool for
system configuration and reproducibility, Guix inspires many ideas about the
possible use cases and further advancement of personal computing experience.
During the presentation various ideas will be presented in the form of
theoretical discussion and will pose more questions than answers."
               15
               (date "2022-09-18 10:15")
               "01-krajnikovszkij.pdf"
               #:video "26-krajnikovszkij.mp4")
         (talk "Guile + Wlroots"
               (list (speaker "Emma Turner" #f #f))
               ""
               10
               (date "2022-09-17 17:30")
               #:video "22-turner.mp4")
         (talk "Guix on OpenPOWER/AArch64"
               (list (speaker "Tobias Platen" #f #f))
               ""
               10
               (date "2022-09-17 17:40")
               #:video "23-platen.mp4")
         (talk "Emacs, Debbugs, and public-inbox"
               (list (speaker "Simon Tournier" #f #f))
               ""
               10
               (date "2022-09-17 17:50")
               #:video "24-tournier.mp4")
         (talk "Remote pair programming"
               (list (speaker "Jérémy Korwin-Zmijowski" #f #f)
                     (speaker "Simon Tournier" #f #f))
               ""
               10
               (date "2022-09-17 18:00")
               #:video "25-korwin-zmijowski-tournier.mp4")

         (talk "Explore your system"
               (list (speaker "Ludovic Courtès" #f "Inria"))
               ""
               10
               (date "2022-09-18 14:30")
               #:video "31-courtes.mp4")
         (talk "L'Union Qiuy Fait La Force"
               (list (speaker "indieterminacy" #f #f))
               ""
               10
               (date "2022-09-18 14:40")
               #:video "32-indieterminacy.mp4")
         (talk "Guix REPL—to infinity and beyond"
               (list (speaker "Simon Tournier" #f #f))
               ""
               10
               (date "2022-09-18 14:50")
               #:video "33-tournier.mp4")
         (talk "Updating a package"
               (list (speaker "Julien Lepiller" #f #f))
               ""
               10
               (date "2022-09-18 15:00")
               #:video "34-lepiller.mp4"))
   talk-earlier?))


;;;
;;; Helper procedures.
;;;

(define (lightning-talk? talk)
  (<= (talk-duration talk) 10))
(define full-length-talk? (negate lightning-talk?))

(define (talk-day-predicate day)
  (lambda (talk)
    (let ((date (talk-date talk)))
      (and (= (date-day date) day)
           (= (date-month date) 09)
           (= (date-year date) 2022)))))

(define friday-talk? (talk-day-predicate 16))
(define saturday-talk? (talk-day-predicate 17))
(define sunday-talk? (talk-day-predicate 18))

(define (talk-range-predicate start end)
  (match start
    ((start-hour start-minute)
     (match end
       ((end-hour end-minute)
        (lambda (talk)
          (let ((date (talk-date talk)))
            (and (or (> (date-hour date) start-hour)
                     (and (= (date-hour date) start-hour)
                          (>= (date-minute date) start-minute)))
                 (or (< (date-hour date) end-hour)
                     (and (= (date-hour date) end-hour)
                          (<= (date-minute date) end-minute)))))))))))

(define morning-talk? (talk-range-predicate '(08 30) '(12 30)))
(define afternoon-talk? (negate morning-talk?))

(define (conjoin . procs)
  (let ((procs (reverse procs)))
    (lambda (arg)
      (every (lambda (proc)
               (proc arg))
             procs))))

(define (speakers->sxml speakers)
  (match speakers
    ((one)
     (if (speaker-affiliation one)
         (string-append (speaker-name one) " ("
                        (speaker-affiliation one) ")")
         (speaker-name one)))
    ((_ ...)
     (let ((affiliations (delete-duplicates
                          (filter-map speaker-affiliation speakers))))
       (string-append (string-join (map speaker-name speakers) ", ")
                      (match affiliations
                        (() "")
                        (_ (string-append " ("
                                          (string-join affiliations ", ")
                                          ")"))))))))

(define (talk-id talk)
  (list->string
   (string-fold-right (lambda (chr lst)
                        (if (char-set-contains? char-set:letter+digit chr)
                            (cons (char-downcase chr) lst)
                            (match lst
                              ((#\- _ ...) lst)   ;don't repeat dashes
                              (_ (cons #\- lst)))))
                      '()
                      (talk-title talk))))

(define (talk->sxml talk)
  `(li (@ (class ,(if (lightning-talk? talk)
                      "lightning talk"
                      "talk"))
          (id ,(talk-id talk)))
       (div (@ (class "talk-time"))
            ,(date->string (talk-date talk) "~H:~M")
            "–"
            ,(let* ((start (talk-date talk))
                    (end   (talk-end-date talk)))
               (date->string end "~H:~M")))
       (div (@ (class "talk-title"))
            (a (@ (href ,(string-append "#" (talk-id talk))))
               ,(talk-title talk)))
       (div (@ (class "talk-speakers"))
            ,(speakers->sxml (talk-speakers talk)))
       ,(match (talk-slides talk)
          (#f "")
          (slides
           `(a (@ (href ,(string-append "/static/slides/" slides))
                  (class "talk-slides"))
               "📁")))
       ,(match (talk-video talk)
          (#f "")
          (video
           `(a (@ (href ,(string-append "/video/" (talk-id talk)))
                  (class "talk-slides"))
               "📺")))
       ,@(match (talk-synopsis talk)
           ((or "" #f) '())
           (_ `((details
                 (summary "Read more...")
                 (div (@ (class "talk-synopsis"))
                      ,@(match (talk-synopsis talk)
                          ((lst ...) lst)
                          ((? string? synopsis) (list synopsis))))))))))

(define* (talk->video-page talk #:key previous next)
  "Return a video page (content and metadata as an alist) for TALK."
  `((title . ,(string-append (talk-title talk) " — "
                             (string-join (map speaker-name
                                               (talk-speakers talk))
                                          ", ")))
    (content
     (div (@ (class "talk-time"))
          ,(date->string (talk-date talk) "~e ~B ~Y, ~H:~M"))
     ,@(if previous
           `((div (@ (class "talk-time"))
                  (a (@ (class "talk-slides")
                        (href ,(string-append "/video/"
                                              (talk-id previous))))
                     "⏪")))
           '())
     ,@(if next
           `((div (@ (class "talk-time"))
                  (a (@ (class "talk-slides")
                        (href ,(string-append "/video/"
                                              (talk-id next))))
                     "⏩")))
           '())

     (div (@ (class "talk-title"))
          (a (@ (href ,(string-append "/program/#" (talk-id talk))))
             ,(talk-title talk)))
     (div (@ (class "talk-speakers"))
          ,(speakers->sxml (talk-speakers talk)))
     ,(match (talk-slides talk)
        (#f "")
        (slides
         `(div (a (@ (href ,(string-append "/static/slides/" slides))
                     (class "talk-slides"))
                  "📁"))))

     (video (@ (class "full-width")
               (controls #t)
               (width "800")
               (height "450")
               (poster "/static/images/video-poster.png"))
            ;; TODO: Replace with a local URL reference.
            (source
             (@ (src ,(string-append
                       "https://guix.gnu.org/guix-videos/ten-years-of-guix/"
                       (talk-video talk))))))

     (div (@ (class "talk-synopsis"))
          ,@(match (talk-synopsis talk)
              ((or "" #f) '())
              ((lst ...) lst)
              ((? string? synopsis) (list synopsis))))

     (blockquote
      (em "Videos published under "
          (a (@ (href "https://creativecommons.org/licenses/by/3.0/"))
             "CC-BY 3.0")
          ".  Brought to you thanks to the support of the Debian video team.
Guix graphics by Luis Felipe.")))))
