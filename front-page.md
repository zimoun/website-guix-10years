title: 16–18th September 2022, Paris, France
frontpage: yes
---

> _The event is now over—thanks to everyone who participated!  Videos
> of the talks are [available from the program page](/program)._

It’s been [ten years of
GNU Guix](https://guix.gnu.org/en/blog/2022/10-years-of-stories-behind-guix/)!
To celebrate, and to share knowledge and enthusiasm, a birthday event
will take place on **September 16–18th, 2022**, in [Paris,
France](/venue).

![Slicing the birthday cake.](/static/images/slicing-the-cake.jpg)

# Program

The [program](/program) spans three days, from Friday to Sunday, 9AM to 6PM.

## Friday: Reproducible deployment for reproducible research

[This day](/program/#Friday) is dedicated to reproducible software deployment as a
foundation for **reproducible research workflows**, with experience
reports from scientists and practitioners, using Guix and other tools.

## Saturday: Hacking (with) Guix

[Saturday](/program/#Saturday) is for **Guix and free software enthusiasts**, users and
developers alike.  We will reflect on ten years of Guix, show what it
has to offer, and present on-going developments and future directions.

## Sunday: Hands-on!

[On Sunday](/program/#Sunday), users and developers (or developers-to-be) will **discuss
technical and community topics** and join forces for hacking sessions,
[unconference style](https://en.wikipedia.org/wiki/Unconference).

# Registration

Attendance is free—as in “freedom” and as in “gratis”—but we kindly ask
you to register so we have
an estimate of how many of us will be eating the birthday cake.

Registration for the in-person event is now closed, but talks will be
[live-streamed](/live).

# Code of Conduct

This online conference is an official Guix event.  Therefore, the [Code of
Conduct](http://git.savannah.gnu.org/cgit/guix.git/tree/CODE-OF-CONDUCT)
applies.  Please be sure to read it beforehand!

If you witness violations of the code of conduct during the event,
please get in touch with the organizers or email
`guix-birthday-event@gnu.org`, a private email alias that reaches the
organizers and the GNU Guix maintainers.

# Contact

For inquiries, please email `guix-birthday-event@gnu.org`, a private
alias that reaches the organizers: Ludovic Courtès, Simon Tournier, and
Tanguy Le Carrour.
