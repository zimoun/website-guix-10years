;;; This module is part of the Ten Years of Guix web site and is licensed
;;; under the same terms, those of the GNU GPL version 3 or (at your option)
;;; any later version.
;;;
;;; Copyright © 2022 Ludovic Courtès <ludo@gnu.org>

`((title . "Sponsors")
  (content

   (img (@ (class "inline-logo")(src "/static/images/logos/irill.png") (alt "IRILL logo")))

   (p (a (@ (href "https://www.irill.org/")) "IRILL") " is the French Center for Research and Innovation on Free
Software (" (i "Initiative de Recherche et Innovation sur le Logiciel Libre") ").
Its mission is to bring together in one place leading researchers and
scientists, expert free and “open source” (FOSS) developers, and FOSS
industry players to tackle the fundamental challenges that FOSS poses in
science, in education, and from an economic standpoint.")

   (img (@ (class "inline-logo")(src "/static/images/logos/software-heritage.png") (alt "Software Heritage logo")))

   (p (a (@ (href "https://www.softwareheritage.org/")) "Software Heritage")
      " is a non-profit, multi-stakeholder organization whose ambition is to
collect, preserve, and share all software that is publicly available in
source code form.  On this foundation, a wealth of applications can be built,
ranging from cultural heritage to industry and research.")

   (img (@ (class "inline-logo")(src "/static/images/logos/inria.png") (alt "Inria logo")))

   (p (a (@ (href "https://www.inria.fr/en")) "Inria") " is the French
research institute in computer science and mathematics.  More
than 3,500 researchers and engineers in 200 project-teams explore new
directions, often in a multidisciplinary context and with industrial and
academic partnerships.  Inria supports innovation through “open source”
software and tech startups.")

   (img (@ (class "inline-logo")(src "/static/images/logos/gricad.png") (alt "GRICAD logo")))

   (p (a (@ (href "https://gricad.univ-grenoble-alpes.fr"))
         "“L’Unité d’Appui à la Recherche” GRICAD") ", co-supervised by
Université Grenoble Alpes, CNRS, Inria, and Grenoble-INP is a structure
at the service of all Grenoble research laboratories (and their external
collaborators). GRICAD provides support and advice to researchers on
their computation, software development processes and data needs. GRICAD
also make available advanced and shared infrastructures for intensive
computing, cloud computing, artificial intelligence and the exploitation
of research data to all researchers and staff in support of research.")

   (img (@ (class "inline-logo")(src "/static/images/logos/meso-lr.jpg") (alt "Meso@LR logo")))

   (p (a (@ (href "https://meso-lr.umontpellier.fr/")) "Meso@LR")
      " is a high-performance
computing (HPC) center (" (i "mésocentre") ") in the city of Montpellier,
offering shared resources and cutting-edge architectures for HPC and big
data.  It is open to both academic and industrial partners.")

   (img (@ (class "inline-logo")(src "/static/images/logos/guix.png") (alt "GNU Guix logo.")))

   (p "The " (a (@ (href "https://guix.gnu.org")) "GNU Guix Project")
      " contributes to this event thanks to donations received over the
years.  The " (a (@ (href "http://foundation.guix.info")) "Guix Europe")"
 non-profit, registered in France (" (i "association loi 1901")"), is dedicated to promoting and supporting Guix and contributes to the event.")))
