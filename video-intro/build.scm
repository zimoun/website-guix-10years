;;; This module extends GNU Guix and is licensed under the same terms, those
;;; of the GNU GPL version 3 or (at your option) any later version.
;;;
;;; Copyright © 2022 Inria

;; To build the video intro slides, run:
;;
;;   guix build -f build.scm
;;
;; The result will be a directory containing the PNG files, one per talk.

(load "../program.scm")

(use-modules (guix) (gnu)
             (program)
             (srfi srfi-1)
             (srfi srfi-19)
             (ice-9 match))

(define font-fira-sans (specification->package "font-fira-sans"))

(define (svg->png svg name)
  (let ((inkscape (specification->package "inkscape")))
    (computed-file name
                   #~(begin
                       ;; Tell Inkscape where to look for fonts.
                       (setenv "XDG_DATA_DIRS"
                               #+(file-append font-fira-sans "/share"))

                       (unless (zero? (system*
                                       #+(file-append inkscape "/bin/inkscape")
                                       "--export-png=t.png" #$svg))
                         (error "inkscape failed!"))
                       (copy-file "t.png" #$output)))))

(define* (instantiated-template template
                                #:key (title "Title")
                                (speakers "Speakers")
                                (affiliations "")
                                (date "date"))
  (define glibc-locales
    (specification->package "glibc-locales"))

  (define build
    (with-imported-modules '((guix build utils))
      #~(begin
          (use-modules (guix build utils)
                       (ice-9 match))

          (define (normalize str)
            (list->string
             (string-fold-right (lambda (chr lst)
                                  (match chr
                                    (#\newline (cons #\space lst))
                                    (#\& (append (string->list "&amp;") lst))
                                    (_ (cons chr lst))))
                                '()
                                str)))

          (setenv "GUIX_LOCPATH"
                  #+(file-append glibc-locales "/lib/locale"))
          (setlocale LC_ALL "en_US.utf8")
          (copy-file #$template #$output)
          (make-file-writable #$output)
          (substitute* #$output
            (("@TITLE@") (normalize #$title))
            (("@SPEAKERS@")
             (string-append (normalize #$speakers)
                            #$(if (string-null? affiliations)
                                  ""
                                  #~(string-append
                                     "\n"
                                     (normalize #$affiliations)))))
            (("@DATE@") #$date)))))

  (computed-file "intro.svg" build))

(define (talks->png-directory talks)
  (define build
    (with-imported-modules '((guix build utils))
      #~(begin
          (use-modules (guix build utils)
                       (srfi srfi-1))

          (mkdir #$output)
          (for-each (lambda (png n)
                      (copy-file png
                                 (string-append #$output "/"
                                                (string-pad
                                                 (number->string n) 2
                                                 #\0)
                                                ".png")))
                    '#$(map (lambda (talk)
                              (svg->png
                               (instantiated-template
                                (local-file "template.svg")
                                #:title (talk-title talk)
                                #:speakers
                                (string-join
                                 (map speaker-name (talk-speakers talk))
                                 ", ")
                                #:affiliations
                                (string-join
                                 (delete-duplicates
                                  (filter-map speaker-affiliation
                                              (talk-speakers talk)))
                                 ", ")
                                #:date (date->string (talk-date talk)
                                                     "~e ~B ~Y"))
                               "intro.png"))
                            talks)
                    (iota 100 1)))))

  (computed-file "video-intros" build))

(talks->png-directory talks)
