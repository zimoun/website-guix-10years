;;; Adapted from <https://hpc.guix.info>
;;;
;;; Copyright © 2022 Simon Tournier <zimon.toutoune@gmail.com>

(add-to-load-path (dirname (assq-ref (current-source-location) 'filename)))

;; This is a build file for Haunt.  Run 'haunt build' to build the web site,
;; and 'haunt serve' to serve it locally.
(use-modules (haunt builder assets)
             (haunt builder blog)
             (haunt html)
             (haunt page)
             (haunt artifact)
             (haunt post)
             (haunt reader commonmark)
             (haunt reader)
             (haunt utils)
             (haunt site)

             (srfi srfi-1)
             (srfi srfi-11)             ;let-values
             (srfi srfi-26)
             (ice-9 match)

             (program))                           ;the conference program


(when (or (getenv "WEB_SITE_LOCAL")
          (member "serve" (command-line))) ; 'haunt serve' command
  ;; The URLs produced in these pages are only meant for local consumption.
  (format #t "~%Producing Web pages for local tests *only*!~%~%"))

(define %web-root
  (or (getenv "GUIX_10Y_WEB_ROOT") ""))           ;empty string means "/"

(define %guix-10-years "Ten Years of Guix")
(define (guix-10-years str)
  (string-append %guix-10-years " — " str))


;;;
;;; Helpers
;;;

(define (base-url . location)
  (string-concatenate (cons %web-root location)))

(define (image-url location)
  (base-url "/static/images" location))

(define (css-url location)
  (base-url "/static/css" location))

(define (full-width-images sxml)
  "Add the 'full-width' class attribute to all 'img' tags of SXML so that
they get properly displayed in blog articles."
  (let loop ((sxml sxml))
    (match sxml
      (('img ('@ attributes ...) rest ...)
       `(img (@ (class "full-width") ,@attributes)
             ,@rest))
      (((? symbol? tag) ('@ attributes ...) rest ...)
       `(,tag (@ ,@attributes) ,@(map loop rest)))
      (((? symbol? tag) rest ...)
       `(,tag ,@(map loop rest)))
      ((lst ...)
       (map loop lst))
      (x x))))


;;;
;;; Layout
;;;

(define* (base-layout body #:key (title %guix-10-years) (meta '())
                      (posts '()) site
                      (extra-css '())
                      (official? (match (getenv "GUIX_10Y_WEB_ROOT")
                                   ((or #f "/" "") #t)
                                   (_ #f))))
  `((doctype "html")
    (html (@ (lang "en"))
          (head
           (meta (@ (http-equiv "Content-Type")
                    (content "text/html; charset=utf-8")))
           (link (@ (rel "icon")
                    (type "image/x-icon")
                    (href ,(image-url "/favicon.png"))))
           (link (@ (rel "stylesheet")
                    (href ,(css-url "/main.css"))
                    (type "text/css")
                    (media "screen")))
           ,@(map (lambda (url)
                    `(link (@ (rel "stylesheet")
                              (href ,url)
                              (type "text/css"))))
                  extra-css)
           (title ,title))
          (body
           (div (@ (id "header")
                   (class "frontpage"))
                (div (@ (id "header-inner")
                        (class "width-control"))
                     (a (@ (href ,(base-url "/")))
                        (img (@ (class "logo")
                                (src ,(if (assoc-ref meta 'frontpage)
                                          (image-url "/logo.gif")
                                          (image-url "/10-years-banner.png"))))))
                     (div (@ (class "baseline"))
                          ,(guix-10-years
                            "16–18 September 2022, Paris, France"))))
           (div (@ (id "menubar")
                   (class "width-control"))
                (ul
                 (li (a (@ (href ,(base-url "/program")))
                        "Program"))
                 (li (a (@ (href ,(base-url "/venue")))
                        "Venue"))
                 (li (a (@ (href ,(base-url "/photos")))
                        "Photos"))
                 (li (a (@ (href ,(base-url "/sponsors")))
                        "Sponsors"))))

           (div (@ (id "content")
                   (class "width-control"))
                (div (@ (id "content-inner"))
                     (article
                      ,(if official?
                           body
                           (list
                            `(h1 (bold "This is a preview; check out the "
                                       (a (@ (href "https://10years.guix.gnu.org"))
                                          "official web site")) ".")
                            body)))))

           (div (@ (id "sponsors"))
                (a (@ (href "/sponsors"))
                   (div (@ (id "sponsors-inner"))
                        (div (@ (class "sponsored-by")) "Sponsors")
                        (div (@ (class "members"))
                             (ul
                              (li (img (@ (alt "IRILL")
                                          (src ,(image-url "/logos/irill.png")))))
                              (li (img (@ (alt "Software Heritage")
                                          (src ,(image-url "/logos/software-heritage.png")))))
                              (li (img (@ (alt "Inria")
                                          (src ,(image-url "/logos/inria.png")))))
                              (li (img (@ (alt "GRICAD")
                                          (src ,(image-url
                                                 "/logos/gricad.png")))))
                              (li (img (@ (alt "Meso@LR")
                                          (src ,(image-url "/logos/meso-lr.jpg")))))
                              (li (img (@ (alt "Guix")
                                          (src ,(image-url
                                                 "/logos/guix.png"))))))))))

           (div (@ (id "footer-box")
                   (class "width-control"))
                (p (a (@ (href "https://gitlab.com/zimoun/website-guix-10years"))
                      "Source of this site")))))))


;;;
;;; Theme
;;;

(define %hpc-haunt-theme
  ;; Theme for the rendering of the news pages.
  (theme #:name %guix-10-years
         #:layout (lambda (site title body)
                    (base-layout body
                                 #:title (guix-10-years title)))))


;;;
;;; Pages
;;;

(define %cwd
  (and=> (assq-ref (current-source-location) 'filename)
         dirname))

(define read-markdown
  (reader-proc commonmark-reader))

(define* (read-page read file posts site #:key (extra-css '()))
  "Read page content from FILE using READ.  Return its final SXML
representation."
  (let-values (((meta body)
                (read (string-append %cwd "/" file))))
    (base-layout `(div (@ (class "post"))
                       (div (@ (class "post-body"))
                            ,body))
                 #:title (guix-10-years (assoc-ref meta 'title))
                 #:meta meta
                 #:posts posts
                 #:extra-css extra-css
                 #:site site)))

(define read-markdown-page
  (cut read-page read-markdown <> <> <>))
(define read-sxml-page
  (cute read-page (reader-proc sxml-reader) <> <> <>))

(define (video-pages talks)
  "Return video pages, one for each element of TALKS."
  (define talk-with-video? talk-video)

  (let loop ((talks talks)
             (previous #f)
             (pages '()))
    (match talks
      (()
       (reverse pages))
      (((? talk-with-video? talk) . rest)
       (let ((next (find talk-with-video? rest)))
         (loop rest talk
               (cons (lambda (site posts)
                       (serialized-artifact
                        (string-append "video/" (talk-id talk)
                                       "/index.html")
                        (let* ((page (talk->video-page talk
                                                       #:previous previous
                                                       #:next next))
                               (body (assq-ref page 'content))
                               (meta (fold alist-delete page '(content))))
                          (base-layout `(div (@ (class "post"))
                                             (div (@ (class "post-body"))
                                                  ,body))
                                       #:title (guix-10-years (assoc-ref meta 'title))
                                       #:meta meta
                                       #:posts posts
                                       #:site site))
                        sxml->html))
                     pages))))
      ((_ . rest)
       (loop rest previous pages)))))

(define (static-pages)
  (define (markdown-page html md)
    (lambda (site posts)
      (make-page html (full-width-images (read-markdown-page md posts site))
                 sxml->html)))

  (append (list (lambda (site posts)
                  (make-page "venue/index.html"
                             (read-sxml-page "venue.sxml" posts site)
                             sxml->html))
                (lambda (site posts)
                  (make-page "live/index.html"
                             (read-page (reader-proc sxml-reader)
                                        "live.sxml" posts site
                                        #:extra-css
                                        '("/static/video-js/7.20.2/video-js.css"))
                             sxml->html))
                (lambda (site posts)
                  (make-page "program/index.html"
                             (read-sxml-page "program.sxml" posts site)
                             sxml->html))
                (lambda (site posts)
                  (make-page "sponsors/index.html"
                             (read-sxml-page "sponsors.scm" posts site)
                             sxml->html))
                (markdown-page "photos/index.html" "photos.md")
                (markdown-page "index.html" "front-page.md"))
          (video-pages talks)))



;;;
;;; Site
;;;

(site #:title %guix-10-years
      #:domain "10years.guix.gnu.org"
      #:default-metadata
      '((author . "Guix Hackers")
        (email  . "guix-birthday-event@gnu.org"))
      #:readers (list commonmark-reader)
      #:builders
      (cons*
       (static-directory "static")
       (static-pages)))
